require('./style.css');
require('../common/common.css');


window.addAnimation = function addAnimation(container) {
    $(container).removeClass('running').delay(100).queue(
        function (next) {
            $(container).addClass('running');
            next();
        }
    );
    return false;
}
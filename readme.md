This paper introduces several demos about CSS 3 animation.

To run this demo

1. npm install

2. npm run 'demo-name'

'demo-name' could be any of the following words

1. cloudy

2. robot

3. shakeHead

4. loadingBar

5. loadingProgress

6. loadingPulse

7. transform


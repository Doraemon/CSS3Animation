var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');
var ENV = '';

if (process.env.ENV) {
    ENV = process.env.ENV;
}

module.exports = {
    context: __dirname,
    //页面入口文件配置
    entry: {
        index: __dirname + '/' + ENV + '/index.js'
    },
    //入口文件输出配置
    output: {
        path: __dirname + '/' + ENV,
        filename: 'bundle.js'
    },
    module: {
        //加载器配置
        rules: [{
                test: /\.css$/,
                use: [{
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    }
                ]
            },
            {
                test: /\.styl$/,
                use: [{
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'stylus-loader'
                    }
                ]
            },
            {
                test: /\.(eot|woff|woff2|gif|png|svg|ttf|jpg)(\?v=(\d|\.)*)*$/,
                use: [{
                    loader: 'file-loader'
                }]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new OpenBrowserPlugin({
            url: 'http://localhost:8889'
        })
    ]
};
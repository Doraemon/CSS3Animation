require('../common/common.css');
require('./style.css');

window.addAnimation = function addAnimation(container) {
    $(container).removeClass('ins').delay(100).queue(
        function (next) {
            $(container).addClass('ins');
            next();
        }
    );
    return false;
}
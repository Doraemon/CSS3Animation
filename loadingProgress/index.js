require('../common/common.css');
require('./style.css');

window.addAnimation = function addAnimation(container) {
    $(container).removeClass('fullwidth').delay(100).queue(
        function (next) {
            $(container).addClass('fullwidth');
            next();
        }
    );
    return false;
}
require('../common/common.css');
require('./style.css');

window.playState = function (container) {
    if ($('i').css("animation-play-state") === 'running') {
        $('i').css("animation-play-state", "paused");
    } else {
        $('i').css("animation-play-state", "running");
    }
    console.log($('i').css("transform"));
}